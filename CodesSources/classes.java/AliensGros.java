
public class AliensGros extends Aliens {

	private boolean rightDirection = false;
	private boolean leftDirection = false;
	private boolean upDirection = false;
	private boolean downDirection = false;

	public AliensGros(Engine engine, Point point, Boolean rightDirection, Boolean leftDirection, Boolean upDirection,
			Boolean downDirection) {
		super(engine, point);
		// TODO Auto-generated constructor stub
		this.setHealth(4);
		this.witdh = 100;
		this.height = 102;
		this.upDirection = upDirection;
		this.downDirection = downDirection;
		this.rightDirection = rightDirection;
		this.leftDirection = leftDirection;
		this.vitesseEntités = 4;
	}

	public boolean isRightDirection() {
		return rightDirection;
	}

	public void setRightDirection(boolean rightDirection) {
		this.rightDirection = rightDirection;
	}

	public boolean isLeftDirection() {
		return leftDirection;
	}

	public void setLeftDirection(boolean leftDirection) {
		this.leftDirection = leftDirection;
	}

	public boolean isUpDirection() {
		return upDirection;
	}

	public void setUpDirection(boolean upDirection) {
		this.upDirection = upDirection;
	}

	public boolean isDownDirection() {
		return downDirection;
	}

	public void setDownDirection(boolean downDirection) {
		this.downDirection = downDirection;
	}

}
