
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.RenderingHints.Key;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Engine extends Thread {

	Graph graph;
	JFrame frame;

	// Nos ArrayList contenant les diff�rentes entit�s du jeu
	private ArrayList<Tir> tab_tirs = new ArrayList<Tir>();
	private ArrayList<TirAliens> tab_tirs_aliens = new ArrayList<>();
	private ArrayList<Aliens> tab_aliens_tu�s = new ArrayList<Aliens>();
	private ArrayList<AliensBoss> tab_aliens_boss = new ArrayList<AliensBoss>();
	private ArrayList<BombAliens> tab_bomb_aliens = new ArrayList<>();
	private ArrayList<AliensBasic> tab_aliens = new ArrayList<AliensBasic>();
	private ArrayList<AliensGros> tab_gros_aliens = new ArrayList<AliensGros>();
	private ArrayList<Bonus> tab_bonus = new ArrayList<Bonus>();
	private ArrayList<BonusRafale> tab_bonus_rafale = new ArrayList<BonusRafale>();
	private ArrayList<BonusVie> tab_bonus_vie = new ArrayList<BonusVie>();

	// variables vaisseau
	private Point pointInit = new Point(300, 400);
	private Vaisseau v = new Vaisseau(pointInit);

	// variables de tirs
	private long Interval_tir = 250;
	private long Dernier_tir = 0;
	private int frequenceTirAliens = 200;
	private int ScoreInt=0;
	// variables de scores/niveau
	private int score = 0;
	private int niveauSuivant = 0;
	private int niveauSuivantGlobal = 0;
	private int niveauGlobal =500;
	private int Tic=200;

	// variables alien
	private int timerApparitionAliens = 150;
	private int timerApparitionGrosAliens = 300;
	private int timerApparition = 0;
	private int frequenceBombAliens = 100;

	// Variables bonus
	private int timerBonusRafale = 500;
	private int timerRecupBonusRafale = 2000;
	private int timerRecupBonusVie = 500;
	private int recupBonus = 0;
	private int recupBonusRafale = 0;

	// Autres variables
	private JLabel label = new JLabel(" ");
	private JLabel scoreJLabel = new JLabel(" ");
	private JLabel pointDeVieJLabel = new JLabel(" ");
	private Boolean anim�1 = true;
	private Boolean appuiSurOui = true;
	private Events E = new Events(this);

	// Notre constructeur
	Engine() {

		graph = new Graph();
		frame = new JFrame();

		// titre de la fenetre
		frame.setTitle("Space Invaders");
		// dimensions de la fen�tre
		frame.setSize(1600, 1000);
		// positionne la fen�tre au milieu de l'�cran
		frame.setLocationRelativeTo(null);
		// ferme la fen�tre si on appui sur la croix
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// on ajoute addKey(E) � notre fen�tre
		frame.addKeyListener(E);
		frame.setContentPane(graph);

		frame.setVisible(true);
		frame.setResizable(false);

		// initialisation des caracteristiques de label
		Font police = new Font("Tahoma", Font.BOLD, 16);
		label.setFont(police);
		label.setForeground(Color.red);
		label.setHorizontalAlignment(JLabel.CENTER);
		label.setText("");

		// label score
		scoreJLabel.setFont(police);
		scoreJLabel.setForeground(Color.green);
		Font police2 = new Font("Tahoma", Font.BOLD, 20);

		// label point de vie
		pointDeVieJLabel.setFont(police2);
		pointDeVieJLabel.setForeground(Color.RED);

		// on ajoute nos diff�rents label � la frame
		frame.add(label, BorderLayout.NORTH);
		frame.add(scoreJLabel, BorderLayout.WEST);
		frame.add(pointDeVieJLabel, BorderLayout.CENTER);

		// on initie les entit�s
		init_entit�s();

		// on commence notre boucle infini
		this.start();

		// on refresh l'affichage
		refresh();

	}

	// initialisation des entit�s/vaisseau/
	public void init_entit�s() {

		recupBonus = 0;
		recupBonusRafale = 0;
		Interval_tir = 250;
		Dernier_tir = 0;
		frequenceTirAliens = 200;
		anim�1 = true;
		appuiSurOui = true;
		niveauSuivant = 0;
		niveauSuivantGlobal = 0;
		timerApparitionAliens = 150;
		timerApparitionGrosAliens = 300;
		timerApparition = 0;
		recupBonus = 0;
		timerBonusRafale = 500;
		timerRecupBonusRafale = 2000;
		timerRecupBonusVie = 1500;
		System.out.println("la partie est finie !");
		score = 0;
		niveauSuivant = 0;
		v.setHealth(10);
		frame.remove(label);
		frequenceTirAliens = 200;
		niveauGlobal = 500;
		// on reinitialise nos tableaux d'entit�s
		for (int i = 0; i < tab_aliens.size(); i++) {
			tab_aliens.remove(i);
		}

		for (int i = 0; i < tab_aliens_boss.size(); i++) {
			tab_aliens_boss.remove(i);
		}

		for (int i = 0; i < tab_bonus.size(); i++) {
			tab_bonus.remove(i);
		}
		for (int i = 0; i < tab_bonus_vie.size(); i++) {
			tab_bonus_vie.remove(i);
		}
		for (int i = 0; i < tab_bonus_rafale.size(); i++) {
			tab_bonus_rafale.remove(i);
		}
		for (int i = 0; i < tab_gros_aliens.size(); i++) {
			tab_gros_aliens.remove(i);
		}

		// bornes ou afficher nos aliens
		// position max et min pour la cr�ation des aliens
		double minX = 0;
		double maxX = frame.getWidth() - 50;
		double minY = 0;
		double maxY = frame.getHeight() - 50;

		// on cr�er des aliens qui vont dans la direction haute
		for (int i = 0; i < 3; i++) {

			double xAliens = Math.random() * (maxX - minX);
			double yAliens = 0;
			// Aliens(Engine engine, Point point, Boolean rightDirection,
			// Boolean leftDirection, Boolean upDirection, Boolean
			// downDirection)
			tab_aliens.add(new AliensBasic(this, new Point(xAliens, yAliens), false, false, false, true));

		}

		// position de d�part du vaisseau au milieu de la fen�tre
		int xInit = (frame.getWidth() - v.getWitdh()) / 2;
		int yInit = (frame.getHeight() - v.getHeight()) / 2;
		v.setX(xInit);
		v.setY(yInit);
		// System.out.println("initialisation du vaisseau");
		// System.out.println("v.x ;" + v.getX());
		// System.out.println("v.y ;" + v.getY());

		E.setDownPressed(false);
		E.setLeftPressed(false);
		E.setRightPressed(false);
		E.setUpPressed(false);

		refresh();
		// calcul du temps du syst�me � cet instant
		this.Dernier_tir = System.currentTimeMillis();

		afficher_texte aff = new afficher_texte();
		Thread t = new Thread(aff);
		t.start();
	}

	// notre processus/boucle infinie
	public void run() {
		while (true) {

			frame.addKeyListener(E);
			// s'active quand appui sur go activ� et barre espace
			while (E.getAnim�() && anim�1) {

				creerAliens();
				creer_tirs_aliens();
				collisionAlienVaisseau();
				collisionBonusVaisseau();
				collisionBonusRafaleVaisseau();
				collisionBonusVieVaisseau();
				collisionAliensTirs();
				supprimerTirs();
				deplacementVaisseau();
				deplacementAliens();
				deplacementTir();
				afficherScore();
				afficherPointDeVieVaisseau();
				niveauSuivant();
				supprimerAliens();
				finPartie();

				refresh();

				// recommencer ? oui/non
				while (appuiSurOui = false) {
					frame.addKeyListener(E);
					appuiSurOui = true;
				}

				// timer
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}

			}

			// System.out.println("play");

		}

	}

	private void creerAliens() {

		// bornes ou afficher nos aliens
		// position max et min pour la cr�ation des aliens
		double minX;
		double maxX;
		double minY;
		double maxY;

		// cr�ation d'aliens � intervale r�gulier
		if (timerApparitionAliens <= 0) {
			minX = 0;
			maxX = frame.getWidth() - 50;

			double xAliens = Math.random() * (maxX - minX);
			double yAliens = 0;
			tab_aliens.add(new AliensBasic(this, new Point(xAliens, yAliens), false, false, false, true));
			setTic();
			timerApparitionAliens = Tic;
			System.out.println("le tic est de"+Tic+"et score int est de"+ScoreInt);
		}
		// se d�cr�mente jusqu'a 0
		timerApparitionAliens--;

		if (timerApparitionGrosAliens <= 0 && niveauSuivantGlobal == 1) {
			timerApparitionGrosAliens = 300;
			minX = 0;
			maxX = frame.getWidth() - 50;
			double xAliens = Math.random() * (maxX - minX);
			double yAliens = 0;
			tab_gros_aliens.add(new AliensGros(this, new Point(xAliens, yAliens), false, false, false, true));
		}
		timerApparitionGrosAliens--;
	}

	// g�re l'augmentation de difficult�s et les bonus
	private void niveauSuivant() {

		timerRecupBonusVie--;
		if (timerRecupBonusVie <= 0) {
			tab_bonus_vie.add(new BonusVie(new Point(0, 0), frame));
			timerRecupBonusVie = 3000;
		}

		if (niveauSuivantGlobal == 0) {

			// TODO Auto-generated method stub
			if (score >= 300 && niveauSuivant < 1) {

				niveauSuivant++;
				// creation bonus
				tab_bonus.add(new Bonus(new Point(0, 0), frame));

			}
			if (score >= 1000 && niveauSuivant < 2) {
				for (int i = 0; i < tab_aliens.size(); i++) {
					tab_aliens.get(i).setVitesseEntit�s(tab_aliens.get(i).getVitesseEntit�s() + 1);
				}
				niveauSuivant++;

			}

			if (score >= 900 && niveauSuivant < 3) {
				frequenceTirAliens = frequenceTirAliens - 10;
				niveauSuivant++;

			}
			if (score >= 1200 && niveauSuivant < 4) {
				frequenceTirAliens = frequenceTirAliens - 10;
				niveauSuivant++;
			}
			if (score >= 1500 && niveauSuivant < 5) {
				frequenceTirAliens = frequenceTirAliens - 10;
				niveauSuivant++;
			}
			if (score >= 1800 && niveauSuivant < 6) {
				frequenceTirAliens = frequenceTirAliens - 10;
				niveauSuivant++;
			}
			if (score >= 2100 && niveauSuivant < 7) {
				for (int i = 0; i < tab_aliens.size(); i++) {
					tab_aliens.get(i).setVitesseEntit�s(tab_aliens.get(i).getVitesseEntit�s() + 1);
				}

				niveauSuivant++;
			}

			if (score >= 2500 && niveauSuivant < 8) {
				niveauSuivantGlobal++;
				niveauSuivant++;

				afficher_texte aff = new afficher_texte();
				Thread t = new Thread(aff);
				t.start();
				// creation bonus
				tab_bonus.add(new Bonus(new Point(0, 0), frame));
			}

		}

		if (score >= 3000 && niveauSuivant < 9) {

			niveauSuivant++;

			tab_bonus_rafale.add(new BonusRafale(new Point(0, 0), frame));
			// this.Interval_tir=20;
			timerBonusRafale = 500;
			timerRecupBonusRafale = 2000;

		}

		if (score >= 3500 && niveauSuivant < 10) {
			niveauSuivant++;
			tab_aliens_boss.add(new AliensBoss(this, new Point(800, 100)));
		}

		if (recupBonusRafale >= 1) {
			timerBonusRafale--;
			timerRecupBonusRafale--;

			if (timerBonusRafale <= 0) {
				this.Interval_tir = 250;
				recupBonusRafale = 0;

			}
		}

		if (recupBonusRafale == 0) {
			timerRecupBonusRafale--;
		}

		if (timerRecupBonusRafale <= 0 && recupBonusRafale == 0) {
			tab_bonus_rafale.add(new BonusRafale(new Point(0, 0), frame));

			timerBonusRafale = 500;
			timerRecupBonusRafale = 2000;
		}
		
		

	}

	// affiche le passage de niveau
	public class afficher_texte implements Runnable {
		int a = 0;

		@Override
		public void run() {
			while (a < 1000000) {
				// TODO Auto-generated method stub
				graph.afficher_texte(frame.getWidth() / 2, frame.getHeight() / 2,
						"Niveau " + (niveauSuivantGlobal + 1));
				a++;

			}
		}

	}

	private void afficherScore() {
		// TODO Auto-generated method stub
		scoreJLabel.setText("score: " + score);

	}

	private void afficherPointDeVieVaisseau() {
		pointDeVieJLabel.setText("Point de vie :" + v.getHealth());
		pointDeVieJLabel.setAlignmentX(frame.getHeight() - 50);

	}

	// Notre fonction permettant de savoir quand une partie est finie
	// ou non
	public void finPartie() {

		// si les points de vie du vaisseau sont a zeros, fin de partie
		if (v.getHealth() <= 0) {

			this.anim�1 = false;

			// lancement d'une boite de dialgoue oui/on
			JOptionPane recommencerJop = new JOptionPane();
			int option = recommencerJop.showConfirmDialog(null, "Voulez vous rejouez?", "Rejouez",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

			// si oui on initie le jeu
			if (option == JOptionPane.OK_OPTION) {
				// appuiSurOui = false;
				init_entit�s();
			}

			// si non on quitte le jeu
			if (option == JOptionPane.NO_OPTION) {
				System.exit(MAX_PRIORITY);
			}
		}

	}

	public void supprimerTirs() {
		for (int i = 0; i < tab_tirs.size(); i++) {
			// si le tir d�passe le haut de l'�cran
			// on supprime le tir
			if (tab_tirs.get(i).getY() < 0)
				tab_tirs.remove(i);
		}
		for (int j = 0; j < tab_tirs_aliens.size(); j++) {

			if (tab_tirs_aliens.get(j).getY() > frame.getHeight()) {
				tab_tirs_aliens.remove(j);
			}
		}
		for (int j = 0; j < tab_bomb_aliens.size(); j++) {

			if (tab_bomb_aliens.get(j).getY() > frame.getHeight()) {
				tab_bomb_aliens.remove(j);
			}
		}
	}

	public void deplacementTir() {
		// verifi simplement que notre tableau de tir n'est pas vide
		if (!tab_tirs.isEmpty()) {
			// on parcourt le tableau de tir
			for (int i = 0; i < tab_tirs.size(); i++) {

				// on fait d�placer le tir (image png) en dy<0
				tab_tirs.get(i).setY(tab_tirs.get(i).getY() - 10);

			}
		}

		if (!tab_tirs_aliens.isEmpty()) {
			// on parcourt le tableau de tir
			for (int i = 0; i < tab_tirs_aliens.size(); i++) {

				// on fait d�placer le tir (image png) en dy>0
				tab_tirs_aliens.get(i).setY(tab_tirs_aliens.get(i).getY() + 10);
			}
		}
		if (!tab_bomb_aliens.isEmpty()) {
			// on parcourt le tableau de tir
			for (int i = 0; i < tab_bomb_aliens.size(); i++) {

				// on fait d�placer le tir (image png) en dy>0
				tab_bomb_aliens.get(i).setY(tab_bomb_aliens.get(i).getY() + 5);
			}
		}

	}

	// permet de faire d�placer les aliens de fa�on pr�defini
	public void deplacementAliens() {

		double minX = 0;
		double maxX = frame.getWidth() - 50;
		double minY = 0;
		double maxY = frame.getHeight() - 50;

		for (int i = 0; i < tab_aliens.size(); i++) {

			// Si l'alien va dans la direction droite
			if (tab_aliens.get(i).isRightDirection()) {

				// si on sort du cot� droit, leftDirection devient vrai
				// rightDirection devient faux
				// puis on d�place l'alien vers le bas
				if (tab_aliens.get(i).getX() + 45 > frame.getWidth()) {
					tab_aliens.get(i).setX(0);
					int yAliens = (int) (Math.random() * (maxY - minY));
					tab_aliens.get(i).setY(yAliens);

				} else {
					tab_aliens.get(i).deplacementAliens(tab_aliens.get(i).getVitesseEntit�s(), 0);

				}

			}

			if (tab_aliens.get(i).isLeftDirection()) {

				// si on sort du cot� gauche,
				// puis on d�place l'alien vers la droite
				if (tab_aliens.get(i).getX() < 0) {
					tab_aliens.get(i).setX(frame.getWidth());
					int yAliens = (int) (Math.random() * (maxY - minY));
					tab_aliens.get(i).setY(yAliens);

				} else {
					tab_aliens.get(i).deplacementAliens(-tab_aliens.get(i).getVitesseEntit�s(), 0);

				}

			}

			if (tab_aliens.get(i).isUpDirection()) {

				// si on sort du haut, downdirection
				// up direction devient faux
				// puis on d�place l'alien vers le bas
				if (tab_aliens.get(i).getY() < 0) {
					tab_aliens.get(i).setY(frame.getHeight());
					int xAliens = (int) (Math.random() * (maxX - minX));
					tab_aliens.get(i).setX(xAliens);

				} else {
					tab_aliens.get(i).deplacementAliens(0, -tab_aliens.get(i).getVitesseEntit�s());

				}

			}

			if (tab_aliens.get(i).isDownDirection()) {

				// si on sort du bas, updirection devient vrai
				// down direction devient faux
				// puis on d�place l'alien vers le haut
				if (tab_aliens.get(i).getY() + 41 > frame.getHeight()) {
					tab_aliens.get(i).setY(0);
					int xAliens = (int) (Math.random() * (maxX - minX));
					tab_aliens.get(i).setX(xAliens);
				} else {
					// tab_aliens.get(i).deplacementAliens(0,
					// tab_aliens.get(i).getVitesseEntit�s());
					tab_aliens.get(i).deplacementAliensSuivi(v.posX, v.posY, tab_aliens.get(i).getVitesseEntit�s());
				}

			}
		}

		for (int j = 0; j < tab_aliens_boss.size(); j++) {

			// Si l'alien va dans la direction droite
			if (tab_aliens_boss.get(j).isRightDirection()) {

				// si on sort du cot� droit, leftDirection devient vrai
				// rightDirection devient faux
				// puis on d�place l'alien vers le bas
				if (tab_aliens_boss.get(j).getX() + 45 > frame.getWidth()) {

					tab_aliens_boss.get(j).deplacementAliens(-10, 0);
					tab_aliens_boss.get(j).setLeftDirection(true);
					tab_aliens_boss.get(j).setRightDirection(false);

				} else {
					tab_aliens_boss.get(j).deplacementAliens(10, 0);

				}

			}

			if (tab_aliens_boss.get(j).isLeftDirection()) {

				// si on sort du cot� droit, leftDirection devient vrai
				// rightDirection devient faux
				// puis on d�place l'alien vers le bas
				if (tab_aliens_boss.get(j).getX() + 45 < 0) {

					tab_aliens_boss.get(j).deplacementAliens(10, 0);
					tab_aliens_boss.get(j).setLeftDirection(false);
					tab_aliens_boss.get(j).setRightDirection(true);

				} else {
					tab_aliens_boss.get(j).deplacementAliens(-10, 0);

				}

			}

		}

		for (int i = 0; i < tab_gros_aliens.size(); i++) {
			if (tab_gros_aliens.get(i).isDownDirection()) {

				// si on sort du bas, updirection devient vrai
				// down direction devient faux
				// puis on d�place l'alien vers le haut
				if (tab_gros_aliens.get(i).getY() + 41 > frame.getHeight()) {
					tab_gros_aliens.get(i).setY(0);
					int xAliens = (int) (Math.random() * (maxX - minX));
					tab_gros_aliens.get(i).setX(xAliens);
				} else {
					// tab_gros_aliens.get(i).deplacementAliens(0,tab_gros_aliens.get(i).getVitesseEntit�s()
					// );
					tab_gros_aliens.get(i).deplacementAliensSuivi(v.posX, v.posY,
							tab_gros_aliens.get(i).getVitesseEntit�s());
				}

			}

		}

	}

	public void supprimerAliens() {
		for (int i = 0; i < tab_aliens.size(); i++) {
			if (tab_aliens.get(i).getHealth() <= 0) {
				tab_aliens_tu�s.add(tab_aliens.get(i));
				tab_aliens.remove(i);
				score += 100;
			}
		}
		for (int i = 0; i < tab_gros_aliens.size(); i++) {
			if (tab_gros_aliens.get(i).getHealth() <= 0) {

				tab_aliens_tu�s.add(tab_gros_aliens.get(i));
				tab_gros_aliens.remove(i);
				score += 200;
			}
		}

		for (int i = 0; i < tab_aliens_boss.size(); i++) {
			if (tab_aliens_boss.get(i).getHealth() <= 0) {
				tab_aliens_boss.remove(i);
				score += 1000;
			}

		}
	}

	// Consruit un tir � partir d'un point (de coordonn�e x,y)
	public void creer_tirs(Point point) {

		// si l'interval entre le tir et le dernier tir n'est pas plus petit
		// que notre interval de tir pr�defini alors la fonction ne fait rien

		if (System.currentTimeMillis() - Dernier_tir < Interval_tir) {
			return;
		}

		// si on a attendu assez longtemps, alors on peut cr�er le tir
		// et on enregistre le temps actuel du tir

		Dernier_tir = System.currentTimeMillis();
		if (recupBonus == 0) {
			// ajoute un nouveau tir de coordonn�e point(x,y) dans tab_tirs
			tab_tirs.add(new Tir(point.x + 45, point.y));
		}

		// double tir
		if (recupBonus == 1) {
			tab_tirs.add(new Tir(point.x + v.getWitdh(), point.y));
			tab_tirs.add(new Tir(point.x, point.y));
		}

		// triple tir
		if (recupBonus == 2) {
			tab_tirs.add(new Tir(point.x + v.getWitdh() / 2, point.y));
			tab_tirs.add(new Tir(point.x, point.y));
			tab_tirs.add(new Tir(point.x + v.getWitdh(), point.y));
		}

	}

	// Consruit un tir � partir d'un point (de coordonn�e x,y)
	public void creer_tirs_aliens() {

		// si le score est sup�rieur a 1000 alors les aliens peuvent maintenant
		// tirer
		// tous les intervales de tirs
		if (score >= 1000) {
			// on parcourt le tableau des aliens
			for (int j = 0; j < tab_aliens.size(); j++) {
				// alors si le timer de tir est <=0 on ajoute un tir �
				// l'aliens
				if (tab_aliens.get(j).getFrequenceTirAliens() <= 0) {
					tab_aliens.get(j).setFrequenceTirAliens(frequenceTirAliens);
					tab_tirs_aliens.add(new TirAliens(tab_aliens.get(j).getX() + 20, tab_aliens.get(j).getY()));
				} else {
					tab_aliens.get(j).setFrequenceTirAliens(tab_aliens.get(j).getFrequenceTirAliens() - 1);
				}
			}
		}
		if (score >= 3000) {
			// on parcourt le tableau des aliens
			for (int j = 0; j < tab_aliens.size(); j++) {
				// alors si le timer de tir est <=0 on ajoute un tir �
				// l'aliens
				if (tab_aliens.get(j).getFrequenceBombAliens() <= 0) {
					tab_aliens.get(j).setFrequencebombAliens(frequenceBombAliens);
					tab_bomb_aliens.add(new BombAliens(tab_aliens.get(j).getX() + 20, tab_aliens.get(j).getY()));
				} else {
					tab_aliens.get(j).setFrequencebombAliens(tab_aliens.get(j).getFrequenceBombAliens() - 1);
				}
			}
		}

		for (int i = 0; i < tab_aliens_boss.size(); i++) {
			if (tab_aliens_boss.get(i).getFrequenceTirAliens() <= 0) {
				tab_tirs_aliens.add(new TirAliens(tab_aliens_boss.get(i).getX(), tab_aliens_boss.get(i).getY()));
				tab_aliens_boss.get(i).setFrequenceTirAliens(20);
			} else {
				tab_aliens_boss.get(i).setFrequenceTirAliens(tab_aliens_boss.get(i).getFrequenceTirAliens() - 1);
			}

		}

	}

	public void collisionAliensTirs() {
		// On parcourt le tableau des diff�rents tirs
		for (int j = 0; j < tab_aliens.size(); j++) {

			// On parcourt le tableau des diff�rents aliens
			for (int i = 0; i < tab_tirs.size(); i++) {

				// Si il y a collision
				if (tab_tirs.get(i).collidesWith(tab_aliens.get(j))) {
					System.out.println(" il y a eu collision!tir 2");
					// on supprime le tir
					tab_tirs.remove(i);
					// on enl�ve un point de vie a l'alien
					tab_aliens.get(j).setHealth(tab_aliens.get(j).getHealth() - 1);
				}
			}

		}

		for (int j = 0; j < tab_aliens_boss.size(); j++) {

			// On parcourt le tableau des diff�rents aliens
			for (int i = 0; i < tab_tirs.size(); i++) {

				// Si il y a collision
				if (tab_tirs.get(i).collidesWith(tab_aliens_boss.get(j))) {
					System.out.println(" il y a eu collision!tir 2");
					// on supprime le tir
					tab_tirs.remove(i);
					// on enl�ve un point de vie a l'alien
					tab_aliens_boss.get(j).setHealth(tab_aliens_boss.get(j).getHealth() - 1);
				}
			}

		}

		for (int j = 0; j < tab_gros_aliens.size(); j++) {

			// On parcourt le tableau des diff�rents aliens
			for (int i = 0; i < tab_tirs.size(); i++) {

				// Si il y a collision
				if (tab_tirs.get(i).collidesWith(tab_gros_aliens.get(j))) {
					System.out.println(" il y a eu collision!tir 2");
					// on supprime le tir
					tab_tirs.remove(i);
					// on enl�ve un point de vie a l'alien
					tab_gros_aliens.get(j).setHealth(tab_gros_aliens.get(j).getHealth() - 1);
				}
			}

		}

		// on parcourt le tableau des tirs aliens
		for (int i = 0; i < tab_tirs_aliens.size(); i++) {

			// si il y a collision entre le tir de l'alien et le vaisseau
			if (tab_tirs_aliens.get(i).collidesWith(v)) {
				System.out.println(" il y a eu collision!tir 2");
				// on enl�ve un point de vie au vaisseau
				v.setHealth(v.getHealth() - 1);
				tab_tirs_aliens.remove(i);

			}

		}
		for (int i = 0; i < tab_bomb_aliens.size(); i++) {

			// si il y a collision entre le tir de l'alien et le vaisseau
			if (tab_bomb_aliens.get(i).collidesWith(v)) {
				System.out.println(" il y a eu collision!bomb 2");
				// on enl�ve un point de vie au vaisseau
				v.setHealth(v.getHealth() - 3);
				tab_bomb_aliens.remove(i);

			}

		}
	}

	// d�placement du vaisseau (fl�che gauche, droite, haut bas)
	public void deplacementVaisseau() {

		// fleche gauche
		if (E.getLeft() && -80 < v.getX()) {

			v.setX(v.getX() - 10);

		}

		// fleche gauche
		if (E.getLeft() && -80 >= v.getX()) {

			v.setX(frame.getWidth());

		}

		// fleche haut
		if (E.getUp() == true && 0 < v.getY()) {
			v.setY(v.getY() - 10);
		}

		// fleche droite
		if (E.getRight() && graph.getWidth() > v.getX()) {
			v.setX(v.getX() + 10);

		}
		if (E.getRight() && graph.getWidth() <= v.getX()) {
			v.setX(0);

		}

		// fleche bas
		if (E.getDown() && graph.getHeight() > v.getY() + 75) {
			v.setY(v.getY() + 10);

		}

	}

	// collision alien / vaisseau
	public void collisionAlienVaisseau() {
		for (int i = 0; i < tab_aliens.size(); i++) {
			// si il y a collision entre le vaisseau et l'alien, supprime 2
			// point de vie au deux
			// et si un des deux � ses points de vie � 0 le d�truit}

			if (v.collidesWith(tab_aliens.get(i))) {

				tab_aliens.get(i).setHealth(tab_aliens.get(i).getHealth() - 10);
				v.setHealth(Math.max(0,v.getHealth() - 10));
				System.out.println("point de vie vaisseau :" + v.getHealth());

			}
		}

		for (int i = 0; i < tab_gros_aliens.size(); i++) {
			// si il y a collision entre le vaisseau et l'alien, supprime 2
			// point de vie au deux
			// et si un des deux � ses points de vie � 0 le d�truit}

			if (v.collidesWith(tab_gros_aliens.get(i))) {

				tab_gros_aliens.get(i).setHealth(tab_gros_aliens.get(i).getHealth() - 10);
				v.setHealth(v.getHealth() - 10);
				System.out.println("point de vie vaisseau :" + v.getHealth());

			}
		}

	}

	public void collisionBonusVaisseau() {
		for (int i = 0; i < tab_bonus.size(); i++) {
			// si il y a collision entre le vaisseau et l'alien, supprime un
			// point de vie au deux
			// et si un des deux � ses points de vie � 0 le d�truit}

			if (v.collidesWithBonus(tab_bonus.get(i))) {
				recupBonus++;
				System.out.println("bonus");
				tab_bonus.remove(i);
			}

		}
	}

	public void collisionBonusRafaleVaisseau() {
		for (int i = 0; i < tab_bonus_rafale.size(); i++) {

			if (v.collidesWithBonus(tab_bonus_rafale.get(i))) {
				recupBonusRafale++;
				this.Interval_tir = 20;
				System.out.println("bonus rafale");
				tab_bonus_rafale.remove(i);
			}

		}
	}

	public void collisionBonusVieVaisseau() {
		for (int i = 0; i < tab_bonus_vie.size(); i++) {

			if (v.collidesWithBonus(tab_bonus_vie.get(i))) {

				v.setHealth(v.getHealth() + 1);
				System.out.println("bonus vie");
				tab_bonus_vie.remove(i);
			}

		}
	}

	// refresh permet de redessiner tout ce qu'il y a redessiner
	// sur l'�cran
	public void refresh() {

		// supprime les elements affich�s sur le graph
		graph.clearEntite();

		graph.paint_fond_ecran();
		// dessine le vaisseau
		graph.paint_vaisseau(v);
		// dessiner bonus
		for (int i = 0; i < tab_bonus.size(); i++) {
			graph.afficher_bonus(tab_bonus.get(i).getPoint());
		}
		for (int i = 0; i < tab_bonus_rafale.size(); i++) {
			graph.afficher_bonus_rafale(tab_bonus_rafale.get(i).getPoint());
		}

		for (int i = 0; i < tab_bonus_vie.size(); i++) {
			graph.afficher_bonus_vie(tab_bonus_vie.get(i).getPoint());
		}
		// dessine les tirs
		for (int i = 0; i < tab_tirs.size(); i++) {

			graph.afficher_tirs2(tab_tirs.get(i).get_Coord());
		}
		for (int i = 0; i < tab_bomb_aliens.size(); i++) {

			graph.afficher_bomb(tab_bomb_aliens.get(i).get_Coord());
		}
		for (int i = 0; i < tab_tirs_aliens.size(); i++) {

			graph.afficher_tirs2(tab_tirs_aliens.get(i).get_Coord());
		}

		// dessine les aliens
		for (int i = 0; i < tab_aliens.size(); i++) {
			graph.afficher_aliens(tab_aliens.get(i).getPoint());
		}
		for (int i = 0; i < tab_gros_aliens.size(); i++) {
			graph.afficher_gros_aliens(tab_gros_aliens.get(i).getPoint());
		}

		for (int i = 0; i < tab_aliens_tu�s.size(); i++) {
			if (tab_aliens_tu�s.get(i).getTimerExplosion() <= 100)
				graph.afficher_explosion(tab_aliens_tu�s.get(i).point, tab_aliens_tu�s.get(i).getTimerExplosion());
			tab_aliens_tu�s.get(i).setTimerExplosion(tab_aliens_tu�s.get(i).getTimerExplosion() - 1);
			if (tab_aliens_tu�s.get(i).getTimerExplosion() <= 0) {
				tab_aliens_tu�s.remove(i);
			}
		}

		for (int j = 0; j < tab_aliens_boss.size(); j++) {
			graph.afficher_aliens_boss(tab_aliens_boss.get(j).getPoint());

		}
		graph.repaint();
	}

	/**
	 * @param keyCode
	 */
	public void key(int keyCode) {
		// TODO Auto-generated method stub
		// keyCode : gauche : 37; haut : 38; droite : 39; bas : 40; a : 65;

		System.out.println(keyCode);

		// touche a : tir

		if (keyCode == 65) {
			creer_tirs(new Point(v.getX(), v.getY()));
			System.out.println("un tir a �t� cr�e");

			// Dernier_tir = System.currentTimeMillis();

		}

	}
	public void setTic(){
		if (score<ScoreInt+600&&score>ScoreInt+300){
			Tic=Tic-5;
			ScoreInt=score;
		}
		if (Tic <= 50)
		{Tic=50;}
		
		
	}
}
