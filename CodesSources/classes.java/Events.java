
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;


// Classe g�rant nos �v�nements 

public class Events implements KeyListener, MouseListener {

	Engine Mother;

	private boolean leftPressed = false;
	private boolean rightPressed = false;
	private boolean upPressed = false;
	private boolean downPressed = false;
	private boolean aPressed = false;
	private boolean anim� = true;
	

	public Events(Engine Mother) {
		this.Mother = Mother;
	}
	
	public boolean getAnim�(){
		return anim�;
	}

	public boolean isLeftPressed() {
		return leftPressed;
	}

	public void setLeftPressed(boolean leftPressed) {
		this.leftPressed = leftPressed;
	}

	public boolean isRightPressed() {
		return rightPressed;
	}

	public void setRightPressed(boolean rightPressed) {
		this.rightPressed = rightPressed;
	}

	public boolean isUpPressed() {
		return upPressed;
	}

	public void setUpPressed(boolean upPressed) {
		this.upPressed = upPressed;
	}

	public boolean isDownPressed() {
		return downPressed;
	}

	public void setDownPressed(boolean downPressed) {
		this.downPressed = downPressed;
	}

	public boolean getUp() {
		return upPressed;
	}

	public void setAnim�(boolean anim�) {
		this.anim� = anim�;
	}

	public boolean getLeft() {
		return leftPressed;
	}

	public boolean getRight() {
		return rightPressed;
	}

	public boolean getDown() {
		return downPressed;
	}



	// m�thode appel�e lors d'un appui clavier
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		Mother.key(arg0.getKeyCode());
		// System.out.println(arg0.getKeyCode());

		if (arg0.getKeyCode() == KeyEvent.VK_LEFT)
			leftPressed = true;

		if (arg0.getKeyCode() == KeyEvent.VK_RIGHT)
			rightPressed = true;

		if (arg0.getKeyCode() == KeyEvent.VK_UP)
			upPressed = true;

		if (arg0.getKeyCode() == KeyEvent.VK_DOWN)
			downPressed = true;
		
		if (arg0.getKeyCode() == KeyEvent.VK_A)
			aPressed = true;
		
		if (arg0.getKeyCode()== KeyEvent.VK_SPACE) {
			if (anim� == true) {
				System.out.println("anim� devient false");
				anim� = false;

			}

			else {
				System.out.println("anim� devient true");
				anim� = true;
			}

		}

		
		
	}

	// appel�e lorsqu'on relache une touche
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		Mother.key(arg0.getKeyCode());
		// System.out.println(arg0.getKeyCode());

		if (arg0.getKeyCode() == KeyEvent.VK_LEFT)
			leftPressed = false;

		if (arg0.getKeyCode() == KeyEvent.VK_RIGHT)
			rightPressed = false;

		if (arg0.getKeyCode() == KeyEvent.VK_UP)
			upPressed = false;

		if (arg0.getKeyCode() == KeyEvent.VK_DOWN)
			downPressed = false;
		
		if (arg0.getKeyCode()==KeyEvent.VK_A)
			aPressed = false;
		
	

	}

	public boolean isaPressed() {
		return aPressed;
	}

	public void setaPressed(boolean aPressed) {
		this.aPressed = aPressed;
	}

	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	
	


	

}
