
// Notre classe Vaisseau

public class Vaisseau extends Entités {

	public Vaisseau(Point point) {
		super(point);
		this.setHealth(10);
		this.setVitesseEntités(100);
		this.witdh = 99;
		this.height = 75;
	}

	public boolean collision(Aliens aliens) {
		if (aliens.getPoint().getX() - 25 <= this.getX() && aliens.getPoint().getX() + 25 >= this.getX()
				&& aliens.getPoint().getY() - 25 <= this.getY() && this.getY() <= aliens.getPoint().getY() + 25) {
			aliens.setHealth(aliens.getHealth() - 1);

			return true;
		}
		return false;
	}

}
