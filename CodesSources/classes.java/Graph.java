
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;


// G�re l'affichage de nos diff�rentes images 
public class Graph extends GraphEngine {
	ArrayList<BufferedImage> image_tab = new ArrayList<BufferedImage>();

	Graph() {
		super();

		IO io = new IO();
		// image_tab.add(io.read("player.png"));
		image_tab.add(io.read("playerShip1_red.png"));
		image_tab.add(io.read("enemyBlue1.png"));
		image_tab.add(io.read("laserRed03.png"));
		image_tab.add(io.read("playerShip3_damage1.png"));
		image_tab.add(io.read("star_gold.png"));
		image_tab.add(io.read("enemyRed4.png"));
		image_tab.add(io.read("pill_blue.png"));

		// image_tab.add(io.read("purple.png"));
		image_tab.add(io.read("fond_ecran.jpg"));
		image_tab.add(io.read("playerShip1_damage1.png"));
		image_tab.add(io.read("playerShip1_damage2.png"));
		image_tab.add(io.read("playerShip1_damage3.png"));
		// image_tab.add(io.read("aliens.png"));
		image_tab.add(io.read("powerupRed.png"));
		image_tab.add(io.read("ufoRed.png"));
		image_tab.add(io.read("bomb.png"));

	}

	// dessine le vaisseau
	public void paint_vaisseau(Vaisseau v) {

		paintEntity(image_tab.get(0), v.getX(), v.getY());
	}

	public void paint_fond_ecran() {

		Graphics g = entite.getGraphics();
		g.drawImage(image_tab.get(7), 0, 0, null);
	}

	// dessine une ligne droite (tir) � la position du point prit en argument
	public void afficher_tirs(Point P) {

		Graphics g = entite.getGraphics();
		g.drawLine(P.x, P.y, P.x, 0);

	}

	public void afficher_explosion(Point P, int timer) {
		Graphics g = entite.getGraphics();
		if (10 < timer && timer < 20) {
			g.drawImage(image_tab.get(10), P.x, P.y, null);
		}
		if (5 < timer && timer <= 10) {
			g.drawImage(image_tab.get(9), P.x, P.y, null);
		}

		if (0 < timer && timer <= 5) {
			g.drawImage(image_tab.get(8), P.x, P.y, null);
		}

	}

	public void afficher_tirs2(Point p) {
		Graphics g = entite.getGraphics();
		g.drawImage(image_tab.get(2), p.x, p.y, null);
	}

	public void afficher_bomb(Point p) {
		Graphics g = entite.getGraphics();
		g.drawImage(image_tab.get(13), p.x, p.y, null);
	}

	public void afficher_bonus(Point point) {

		// System.out.println("bonus affich�");
		Graphics g = entite.getGraphics();
		g.drawImage(image_tab.get(4), point.x, point.y, null);
	}

	public void afficher_bonus_rafale(Point point) {

		// System.out.println("bonus affich�");
		Graphics g = entite.getGraphics();
		g.drawImage(image_tab.get(6), point.x, point.y, null);
	}

	// affiche l'alien � la position (x,y) du point prit en argument
	public void afficher_aliens(Point P) {
		Graphics g = entite.getGraphics();
		g.drawImage(image_tab.get(1), P.x, P.y, this);

	}

	public void afficher_gros_aliens(Point point) {
		// TODO Auto-generated method stub

		Graphics g = entite.getGraphics();
		g.drawImage(image_tab.get(5), point.x, point.y, this);

	}

	public void afficher_texte(int x, int y, String texte) {

		Graphics g = entite.getGraphics();
		g.drawString(texte, x, y);
	}

	public void afficher_bonus_vie(Point point) {
		// TODO Auto-generated method stub
		Graphics g = entite.getGraphics();
		g.drawImage(image_tab.get(11), point.x, point.y, this);
	}

	public void afficher_aliens_boss(Point point) {
		// TODO Auto-generated method stub
		Graphics g = entite.getGraphics();
		g.drawImage(image_tab.get(12), point.x, point.y, this);
	}

}