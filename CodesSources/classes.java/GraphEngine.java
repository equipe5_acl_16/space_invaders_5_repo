
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GraphEngine extends JPanel {

	int sizeX = 1600, sizeY = 1000;
	// definitions des buffered images
	protected BufferedImage fond = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_INT_RGB);
	protected BufferedImage entite = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_INT_ARGB);
	ArrayList<BufferedImage> unite_tab = new ArrayList<BufferedImage>();

	// Notre constructeur
	public GraphEngine() {
		repaint();
	}

	public void clearEntite() {
		entite = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_INT_ARGB);

	}

	public void paintComponent(Graphics g) {

		// dessine fond et entit้ sur g
		g.drawImage(fond, 0, 0, this);
		g.drawImage(entite, 0, 0, this);

	}

	public void paintEntity(BufferedImage image, double x, double y) {
		Graphics g = entite.getGraphics();
		// dessine une image sur g
		g.drawImage(image, (int) x, (int) y, this);
	}

}
