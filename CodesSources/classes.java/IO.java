
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;



public class IO {

	//Permet de lire une image
	public BufferedImage read(String path){
		BufferedImage img = null; 
		try{
             
	            img = ImageIO.read(new File(path));
	            
	        } catch(IOException e){
	            e.printStackTrace();
	        }
	            
		return img;
		
	}

	public int getWidth() {

		return this.getWidth();
	}
	
	public int getHeight(){
		return this.getHeight();
	}
		
}
