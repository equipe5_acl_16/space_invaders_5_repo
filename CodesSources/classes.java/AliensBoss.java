
public class AliensBoss extends Aliens {
	boolean rightDirection;
	boolean leftDirection;

	public AliensBoss(Engine engine, Point point) {
		super(engine, point);
		this.setHealth(40);
		this.point = point;
		this.frequenceTirAliens = 20;
		this.vitesseEntités = 1;// TODO Auto-generated constructor stub
		this.rightDirection = true;
		this.leftDirection = false;
		this.witdh = 91;
		this.height = 91;
	}

	public boolean isRightDirection() {
		return rightDirection;
	}

	public void setRightDirection(boolean rightDirection) {
		this.rightDirection = rightDirection;
	}

	public boolean isLeftDirection() {
		return leftDirection;
	}

	public void setLeftDirection(boolean leftDirection) {
		this.leftDirection = leftDirection;
	}

	public void deplacementAliens(int dx, int dy) {
		// TODO Auto-generated method stub
		this.posX = this.posX + dx;
		this.posY = this.posY + dy;
		this.point.x = this.point.x + dx;
		this.point.y = this.point.y + dy;

	}

}
