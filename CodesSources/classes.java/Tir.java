import java.awt.Rectangle;

// Notre classe tir
public class Tir {

	public void setTimer(int timer) {
		this.timer = timer;
	}

	int timer = 0;
	int x;
	int y;
	int witdh = 9;
	int height = 37;
	private int timerExplosion = 50;
	// The rectangle used for this entity during collisions resolution */
	protected Rectangle me = new Rectangle();
	// The rectangle used for other entities during collision resolution */
	protected Rectangle him = new Rectangle();
	// quelTir = 0 : tir vaisseau, quelTir =1 : tir Aliens
	protected int quelTir = 0;

	// instancie nos variables globales x,y
	public Tir(int x, int y) {
		this.x = x;
		this.y = y;

	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	// fonction appel� � chaque frame qui renvoit true si le tir peut
	// disparaitre
	public boolean tic() {

		timer--;
		if (timer <= 0)
			return (true);
		return false;

	}

	public boolean ticExplosion() {
		timerExplosion--;
		if (timerExplosion <= 0)
			return (true);
		return false;
	}

	public boolean collidesWith(Entit�s other) {
		me.setBounds((int) x, (int) y, witdh, height);
		him.setBounds((int) other.posX, (int) other.posY, other.getWitdh(), other.getHeight());

		return me.intersects(him);
	}
	public Point get_Coord() {
		return new Point(x, y);
	}

	// plus utilis�e
	public boolean collision(Aliens aliens, int x_tir) {
		if (aliens.getX() < x_tir && aliens.getX() + 45 > x_tir) {
			aliens.setHealth(aliens.getHealth() - 1);
			System.out.println("Il y a eu collision !");
			return true;
		}
		return false;
	}

}