import java.util.ArrayList;

import javax.swing.JFrame;

public class Aliens extends Entités {

	protected int frequenceTirAliens = 200;
	private int timerExplosion = 20;
	protected int frequenceBombAliens = 100;

	public int getTimerExplosion() {
		return timerExplosion;
	}

	public void setTimerExplosion(int timerExplosion) {
		this.timerExplosion = timerExplosion;
	}

	public int getFrequenceTirAliens() {
		return frequenceTirAliens;
	}

	public int getFrequenceBombAliens() {
		return frequenceBombAliens;
	}

	public int getWitdh() {
		return witdh;
	}

	public int getHeight() {
		return height;
	}

	public Aliens(Engine engine, Point point) {
		super(point);

	}

	public void deplacementAliens(int dx, int dy) {

		this.posX = this.posX + dx;
		this.posY = this.posY + dy;
		this.point.x = this.point.x + dx;
		this.point.y = this.point.y + dy;

	}

	// L'IA pour le déplacement 
	public void deplacementAliensSuivi(int x, int y, int dx) {
		if(y>this.posY+10){
			int distx = x - this.posX;
			int disty = Math.abs(y - this.posY);
			double dist = Math.sqrt(distx * distx + disty * disty);
			double depx = dx * distx / dist;
			double depy = dx * disty / dist;
			this.posX = (int) Math.round(this.posX + depx);
			this.posY = (int) Math.round(this.posY + depy);}
		else{
			this.posX=this.posX;
			this.posY=this.posY+dx;
		}
		if(y>this.point.y+10){
			int disx = Math.abs(x - this.point.x);
			int disy = Math.abs(y - this.point.y);
			double dis = Math.sqrt(disx * disx + disy * disy);
			double deplx = dx * (x - this.point.x) / dis;
			double deply = dx * disy / dis;
			this.point.x = (int) Math.round(this.point.x + deplx);
			this.point.y = (int) Math.round(this.point.y + deply);}
		else{
			this.point.x=this.point.x;
			this.point.y=this.point.y+dx;
		}

	}

	public boolean collision_aliens_vaisseau(Vaisseau v) {

		if (this.getPoint().getX() == v.getX() && this.getPoint().getY() == v.getY()) {
			return true;
		}
		return false;
	}

	public void setFrequenceTirAliens(int frequenceTirAliens) {
		this.frequenceTirAliens = frequenceTirAliens;
	}

	public void setFrequencebombAliens(int frequencebombAliens) {
		this.frequenceBombAliens = frequencebombAliens;
	}

}
