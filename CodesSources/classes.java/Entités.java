import java.awt.Rectangle;

public class Entités {

	protected int health;
	protected int posX;
	protected int posY;
	protected boolean destroyed;

	protected int vitesseEntités;
	protected Point point;
	protected IO sprite;

	protected int witdh;
	protected int height;

	protected int dx;
	protected int dy;

	/** The rectangle used for this entity during collisions resolution */
	protected Rectangle me = new Rectangle();
	/** The rectangle used for other entities during collision resolution */
	protected Rectangle him = new Rectangle();

	public Entités(Point point) {
		this.posX = point.x;
		this.posY = point.y;
		this.point = point;

	}

	public int getVitesseEntités() {
		return vitesseEntités;
	}

	// return true si il y a collision entre les deux rectangles entourant les
	// images
	// png de nos entités
	public boolean collidesWith(Aliens other) {
		me.setBounds((int) posX, (int) posY + 25, witdh, height);
		him.setBounds((int) other.posX, (int) other.posY, other.getWitdh(), other.getHeight());

		return me.intersects(him);
	}

	public boolean collidesWithBonus(Bonus other) {
		me.setBounds((int) posX, (int) posY, witdh, height);
		him.setBounds((int) other.point.x, (int) other.point.y, other.getWitdh(), other.getHeight());

		return me.intersects(him);
	}

	public int getX() {
		return this.posX;
	}

	public int getY() {
		return this.posY;
	}

	public void setX(int x) {
		this.point.x = x;
		this.posX = x;
	}

	public void setY(int y) {
		this.point.y = y;
		this.posY = y;
	}

	public Point getPoint() {
		return this.point;
	}

	public void setPoint(Point point) {
		this.point = point;
		this.posX = point.x;
		this.posY = point.y;
	}

	public void setHealth(int health) {
		this.health = health;
		System.out.println("on a rendu des pdv au vaisseau");
	}

	public int getHealth() {
		return (this.health);
	}

	public void setVitesseEntités(int vitesse) {
		this.vitesseEntités = vitesse;
	}

	public int getWitdh() {
		return witdh;
	}

	public int getHeight() {
		return height;
	}

	// renvoit true si les points de vie de l'entité sont ŕ 0
	public boolean estMort() {

		if (this.health <= 0) {
			System.out.println("l'entité est morte");
			return true;
		}
		return false;
	}

}
